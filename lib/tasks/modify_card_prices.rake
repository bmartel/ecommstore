desc "Update all cards with random values"
task :cards_price_change => :environment do
  Card.all.each do |card|
    card.update_attribute :price, (rand() * (12.75-0.10) + 0.10).round(2)
  end
end