Ccghoard::Application.routes.draw do

  resources :decks
  resources :cards

  authenticated :user do
    root :to => 'cards#index'
  end
  root :to => "cards#index"
  devise_for :users
  resources :users



end