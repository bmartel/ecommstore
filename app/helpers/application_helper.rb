module ApplicationHelper
  #sort column using the table th row as links to sort by
  def sortable(column, title = nil)
    title ||= column.titleize
    css_class = column == sort_column ? "current #{sort_direction}" : nil
    direction = column == sort_column && sort_direction == "asc" ? "desc" : "asc"
    link_to title, params.merge(:sort => column, :direction => direction, :page => nil), {:class => css_class}
  end

  def make_images(file_image_array)

    img_arry = []

    file_image_array.each do |cur_image|
      img_arry << (image_tag "/images/assortedimages/"+cur_image)
    end
    img_arry.join()
  end

end
