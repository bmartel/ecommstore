class HomeController < ApplicationController
  def index
    @cards = Card.paginate(:page => params[:page], :per_page => 30)
  end

  def sort_column
    Card.column_names.include?(params[:sort]) ? params[:sort] : "name"
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end
end
