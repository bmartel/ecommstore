class CardsDecks < ActiveRecord::Base
  attr_accessible :card_id, :deck_id
  belongs_to :cards
  belongs_to :decks

end
