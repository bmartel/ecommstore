class Deck < ActiveRecord::Base
  
  attr_accessible :name, :card_ids
  has_and_belongs_to_many :cards

  def deck_price
    total_cost = 0
    deck_cards = self.where(:deck_id => self.id)
    deck_cards do |c|
      total_cost+=c.price
    end
    total_cost
  end
end
