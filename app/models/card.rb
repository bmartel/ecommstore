class Card < ActiveRecord::Base
  resourcify
  attr_accessible :cardset, :cardtype, :color, :manacost, :description, :image, :name, :powertoughness, :price

  has_and_belongs_to_many :decks


  def self.search(search)
    if search
      where('name LIKE ?', "%#{search}%")
    else
      scoped
    end
  end

  def get_image
    image || 'default.jpg'
  end

  def get_symbols(mana_info)
    #parse the manacost string, look for particular key characters representing the sybmol
    #image in place of the character
    #
    #assuming incoming variable and column value: @card.manacost
    #
    #Syntax Check:
    #card = "(2/U)(2/U)(2/U)"
    #card = "2RB"
    #card = "2(R/U)(R/U)"
    image_array = []
    hybrid_mana = ""
    capture_text = false
    mana_info.split("").each do |symbol|
      image_name = ""
      if symbol == "("
        capture_text = true
        hybrid_mana = ""
      elsif symbol == ")"
        capture_text = false
      else

      end

      if capture_text == true && symbol != "("
        hybrid_mana += symbol
      elsif hybrid_mana != ""
        case hybrid_mana
          #hybrid mana
          when "W/U"
            image_name = "whiteblue.png"
          when "W/B"
            image_name = "whiteblack.png"
          when "U/B"
            image_name = "blueblack.png"
          when "U/R"
            image_name = "bluered.png"
          when "B/R"
            image_name = "blackred.png"
          when "B/G"
            image_name = "blackgreen.png"
          when "R/G"
            image_name = "redgreen.png"
          when "R/W"
            image_name = "redwhite.png"
          when "G/W"
            image_name = "greenwhite.png"
          when "G/U"
            image_name = "greenblue.png"

          #hybrid mana inverse
          when "U/W"
            image_name = "bluewhite.png"
          when "B/W"
            image_name = "blackwhite.png"
          when "B/U"
            image_name = "blackblue.png"
          when "R/U"
            image_name = "redblue.png"
          when "R/B"
            image_name = "redblack.png"
          when "G/B"
            image_name = "greenblack.png"
          when "G/R"
            image_name = "greenred.png"
          when "W/R"
            image_name = "whitered.png"
          when "W/G"
            image_name = "whitegreen.png"
          when "U/G"
            image_name = "bluegreen.png"
          #monocolored hybrid
          when "1/G"
            image_name = "1green.png"
          when "1/U"
            image_name = "1blue.png"
          when "1/R"
            image_name = "1red.png"
          when "1/B"
            image_name = "1black.png"
          when "1/W"
            image_name = "1white.png"
          when "2/G"
            image_name = "2green.png"
          when "2/U"
            image_name = "2blue.png"
          when "2/R"
            image_name = "2red.png"
          when "2/B"
            image_name = "2black.png"
          when "2/W"
            image_name = "2white.png"
          when "3/G"
            image_name = "3green.png"
          when "3/U"
            image_name = "3blue.png"
          when "3/R"
            image_name = "3red.png"
          when "3/B"
            image_name = "3black.png"
          when "3/W"
            image_name = "3white.png"
        end
      else

        #add all checks for every manacost type of every card possible, replace with a image_tag()
        #for the appropriate image of the symbol
        case symbol

          #standard mana types ex. forest, mountain, island, swamp, plain
          when "G"
            image_name = "green.png"
          when "U"
            image_name = "blue.png"
          when "R"
            image_name = "red.png"
          when "B"
            image_name = "black.png"
          when "W"
            image_name = "white.png"

          #colorless mana
          when "0"
            image_name = "0.png"
          when "1"
            image_name = "1.png"
          when "2"
            image_name = "2.png"
          when "3"
            image_name = "3.png"
          when "4"
            image_name = "4.png"
          when "5"
            image_name = "5.png"
          when "6"
            image_name = "6.png"
          when "7"
            image_name = "7.png"
          when "8"
            image_name = "8.png"
          when "9"
            image_name = "9.png"
          when "10"
            image_name = "10.png"
          when "11"
            image_name = "11.png"
          when "12"
            image_name = "12.png"
          when "X"
            image_name = "x.png"
        end
      end
      image_array << image_name unless image_name.empty?
    end
    image_array
  end
end
