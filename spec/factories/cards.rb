# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :card do
    name "MyString"
    cardset "MyString"
    color "MyString"
    cardtype "MyString"
    powertoughness "MyString"
    description "MyText"
    image "MyString"
  end
end
