# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :cards_deck, :class => 'CardsDecks' do
    card_id 1
    deck_id 1
  end
end
